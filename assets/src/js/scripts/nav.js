define([
],function() {

	var 
		$document = $(document),
		$html = $('html'),
		$nav = $('nav'),
		$body = $('body'),
		SHOW_CLASS = 'show-nav';


	//mobile nav
	$document
		.on('click','.toggle-nav',function(e) {
			methods.toggleNav();
			return false;
		})
		.on('click','body',function(e) {
			e.target === $body[0] && methods.isShowingNav() && methods.toggleNav();
		})
		.on('keyup',function(e) {
			e.keyCode === 27 && methods.isShowingNav() && methods.toggleNav();
		});

	var methods = {

		showNav: function(show) {
			var method = 'removeClass';
			
			if(show) {
				method = 'addClass';
				setTimeout(function() {
					$nav.find('a').eq(0).focus();
				},400);
			}
			
			$html[method](SHOW_CLASS);

		},

		toggleNav: function() {
			this.showNav(!this.isShowingNav());
		},

		isShowingNav: function() {
			return $html.hasClass(SHOW_CLASS);
		}

	};
		

	//no public API
	return {};

});