(function() {

		//selector wrapper
		$(document)
			.on('change','div.selector',function(e) {
				var 
					el = $(this),
					select = $('select',el),
					val = $('span.value',el),
					selectedOption = select.children('option:selected'),
					valText = selectedOption.data('tag') || selectedOption.text();

				val.html(valText);

			})

		//setup on page load
		$('div.selector select').trigger('change');
		
}());