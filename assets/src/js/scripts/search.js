define([
],function() {

	var 
		query = '',
		req,
		$html = $('html'),
		$overlay = $('div.search-overlay'),
		$form = $overlay.find('form'),
		$input = $form.children('input'),
		showclass = 'show-search',
		methods = {
		
			showForm: function() {
				$html.addClass(showclass);
				$input.val('');
				Modernizr.touch || $input.focus();
			},
			
			hideForm: function() {
				$html.removeClass(showclass);
				$input.blur();
			},
			
			isVisible: function() {
				return $html.hasClass(showclass);
			},
			
			toggleForm: function() {
				if(!this.isVisible()) {
					return this.showForm();
				}
				
				return this.hideForm();
			}
	
		};
	
	$(document)
		.on('click','.toggle-search-form',function(e) {
			e.preventDefault();
			methods.toggleForm();
		})
		.on('keydown',function(e) {
			e.keyCode === 27 && methods.hideForm();
		});


	return methods;
});