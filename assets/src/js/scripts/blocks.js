define([
],function() {

	$(window)
		.on('load resize',function() {
			var blocks = $('.blocks').filter(function() { return !$(this).hasClass('always-list'); });
			
			if(window.outerWidth < 650) {
				blocks.addClass('list-view');
				return;
			}
			
			blocks.each(function(i,blockGroup) {
				var collapseSize = blockGroup.className.match(/collapse-(\d+)/);
				if(collapseSize.length == 2 && window.outerWidth < (+collapseSize[1])) {
					$(blockGroup).addClass('list-view');
				} else {
					$(blockGroup).removeClass('list-view');
				}
			});
			
		}).trigger('resize');
			
	return {};
});