						<div class="green-bg did-you-know">
							<h3 class="title">Did You Know?</h3>
							
							<div class="swiper-wrapper">
							
								<div class="swipe" data-links="true">
									<div class="swipe-wrap">
										<div>
											<ol>
												<li>
													Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla tempus, mauris quis sodales.
												</li>
												<li>
													Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla tempus, mauris quis sodales.
												</li>
												<li>
													Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla tempus, mauris quis sodales.
												</li>
												<li>
													Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla tempus, mauris quis sodales.
												</li>
											</ol>
										</div>
										<div>
											<ol>
												<li>
													Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla tempus, mauris quis sodales.
												</li>
												<li>
													Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla tempus, mauris quis sodales.
												</li>
												<li>
													Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla tempus, mauris quis sodales.
												</li>
											</ol>
										</div>
									</div><!-- .swipe-wrap -->
								</div><!-- .swipe -->
							</div><!-- .swiper-wrapper -->
							
						</div><!-- .green-bg -->
